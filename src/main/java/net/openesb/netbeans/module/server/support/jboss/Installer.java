package net.openesb.netbeans.module.server.support.jboss;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.openide.modules.Dependency;
import org.openide.modules.ModuleInfo;
import org.openide.modules.ModuleInstall;
import org.openide.modules.Modules;

/**
 * Copy of https://bitbucket.org/jglick/yenta
 * As soon as we are not able to wrap library easily, I prefer to copy the class
 * and embed it directly is the concerned module.
 * 
 * @author David BRASSELY
 */
public class Installer extends ModuleInstall {

    /**
     * Specifies the modules with whom you would like to be friends.
     * These modules must be among your declared dependencies and they must export friend packages.
     * For each such module, if you are not already a friend, you will be treated as one,
     * so you will be able to access friend (but not private) packages.
     * @return a set of module code name bases (default implementation is empty)
     */
    protected Set<String> friends() {
        return Collections.singleton("org.netbeans.modules.j2ee.sun.appsrv");
    }

    /**
     * Specifies the modules from whom you need complete access.
     * These modules must be among your declared dependencies.
     * For each such module, you will be able to access all packages, as with an implementation dependency.
     * Be careful to defend against unexpected signature changes!
     * @return a set of module code name bases (default implementation is empty)
     */
    protected Set<String> siblings() {
        return Collections.singleton("org.netbeans.modules.j2ee.jboss4");
    }

    /**
     * @inheritDoc
     * @throws IllegalStateException if {@link #friends} and {@link #siblings} are misconfigured or if the module system cannot be manipulated
     */
    @Override public void validate() throws IllegalStateException {
        Set<String> friends = friends();
        Set<String> siblings = siblings();
        if (friends.isEmpty() && siblings.isEmpty()) {
            throw new IllegalStateException("Must specify some friends and/or siblings");
        }
        ModuleInfo me = Modules.getDefault().ownerOf(getClass());
        if (me == null) {
            throw new IllegalStateException("No apparent module owning " + getClass());
        }
        try {
            Object manager = me.getClass().getMethod("getManager").invoke(me);
            for (String m : friends) {
                if (siblings.contains(m)) {
                    throw new IllegalStateException("Cannot specify the same module " + m + " in both friends and siblings");
                }
                Object data = data(findDependency(manager, m));
                Field friendNamesF = Class.forName("org.netbeans.ModuleData", true, data.getClass().getClassLoader()).getDeclaredField("friendNames");
                friendNamesF.setAccessible(true);
                Set<?> names = (Set<?>) friendNamesF.get(data);
                Set<Object> newNames = null;
                
                if (names == null) {
                    newNames = new HashSet<Object>();
                } else {
                    newNames = new HashSet<Object>(names);
                }

                newNames.add(me.getCodeNameBase());
                friendNamesF.set(data, newNames);
            }
            for (String m : siblings) {
                ModuleInfo dep = findDependency(manager, m);
                String implVersion = dep.getImplementationVersion();
                if (implVersion == null) {
                    throw new IllegalStateException("No implementation version found in " + m);
                }
                Object data = data(me);
                Field dependenciesF = Class.forName("org.netbeans.ModuleData", true, data.getClass().getClassLoader()).getDeclaredField("dependencies");
                dependenciesF.setAccessible(true);
                Dependency[] dependencies = (Dependency[]) dependenciesF.get(data);
                boolean found = false;
                for (int i = 0; i < dependencies.length; i++) {
                    if (dependencies[i].getName().replaceFirst("/.+$", "").equals(m)) {
                        Set<Dependency> nue = Dependency.create(Dependency.TYPE_MODULE, dependencies[i].getName() + " = " + implVersion);
                        if (nue.size() != 1) {
                            throw new IllegalStateException("Could not recreate dependency from " + dependencies[i] + " based on " + implVersion);
                        }
                        dependencies[i] = nue.iterator().next();
                        found = true;
                    }
                }
                if (!found) {
                    throw new IllegalStateException("Did not find dependency on " + m);
                }
                // StandardModule.classLoaderUp skips adding a parent if the dep seemed to offer us nothing, and this has already been called.
                Object[] publicPackages = (Object[]) dep.getClass().getMethod("getPublicPackages").invoke(dep);
                if (publicPackages != null && publicPackages.length == 0) {
                    me.getClassLoader().getClass().getMethod("append", ClassLoader[].class).invoke(me.getClassLoader(), (Object) new ClassLoader[] {dep.getClassLoader()});
                }
            }
        } catch (IllegalStateException x) {
            throw x;
        } catch (Exception x) {
            throw new IllegalStateException(x);
        }
    }

    private ModuleInfo findDependency(/*ModuleManager*/Object manager, String m) throws Exception {
        Object dep = manager.getClass().getMethod("get", String.class).invoke(manager, m);
        if (dep == null) {
            throw new IllegalStateException("No such dependency " + m);
        }
        return (ModuleInfo) dep;
    }

    private Object data(ModuleInfo module) throws Exception {
        Method dataM = Class.forName("org.netbeans.Module", true, module.getClass().getClassLoader()).getDeclaredMethod("data");
        dataM.setAccessible(true);
        return dataM.invoke(module);
    }

}
