package net.openesb.netbeans.module.server.support.jboss;

import javax.enterprise.deploy.shared.factories.DeploymentFactoryManager;
import javax.enterprise.deploy.spi.factories.DeploymentFactory;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY
 */
public class JBDeploymentFactory extends org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory {
 
    public static final String URI_PREFIX = "openesb:jboss-deployer:"; // NOI18N
    
    private static JBDeploymentFactory instance;

    public static synchronized DeploymentFactory create() {
        if (instance == null) {
            instance = new JBDeploymentFactory();
            DeploymentFactoryManager.getInstance().registerDeploymentFactory(instance);

        //    registerDefaultServerInstance();
        }

        return instance;
    }
    
    @Override
    public String getDisplayName() {
        return NbBundle.getMessage(JBDeploymentFactory.class, "SERVER_NAME"); // NOI18N
    }
    
    @Override
    public boolean handlesURI(String uri) {
        if (uri != null && uri.startsWith(URI_PREFIX)) {
            return true;
        }

        return false;
    }
}
